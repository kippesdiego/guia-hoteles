# guia de Hoteles

## Intalacion
1) npm init
2) Si no lo tengo instalado -> `npm install lite-server --save-dev`
3) Dentro de Script escribir ("dev": "lite-server")
4) Para correr el server `npm run dev`
------------------------------------------------------------------------
5) `npm install bootstrap --save` (requiere jquery y popper)
6) `npm install jquery --save`
7) `npm install popper.js --save`
-------------------------------------------------------------------------
8) Instalar iconos `npm install open-iconic --save`
-------------------------------------------------------------------------
9) npm install node-sass --save-dev
LO MISMO QUE LESS
11) En package, para buscar la carpeta de css
"scss": "node-sass -o css/ css/"
-------------------------------------------------------------------------
12)Para ejecutar el compilador de scss
npm run scss
-------------------------------------------------------------------------
PARA LESS
9)npm install less --save
-------------------------------------------------------------------------
10) cd css, touch style.less
-------------------------------------------------------------------------
11)Compilar less
lessc css/style.less css/style.less
-------------------------------------------------------------------------
AUTOMATIZAR TAREAS
anchange: monitorear cambios en archivos. En package: " "watch:scss": "onchange 'css/*.css -- npm run scss " "
rimraf:
npm install --save-dev onchange rimraf
-------------------------------------------------------------------------
MONITOREAR ARCHIVOS SCSS
npm run watch:scss
-------------------------------------------------------------------------
npm install --save concurrently
-------------------------------------------------------------------------
"start": "concurrently \"npm run watch:scss\" \"npm run dev \""
-------------------------------------------------------------------------
npm run start
-------------------------------------------------------------------------
PARA COPIAR DE ARCHIVOS DE UNA FORMA FACIL
npm install --save-dev copyfiles
-------------------------------------------------------------------------
COMPRIMIR IMAGENES
npm install imagemin --save
-------------------------------------------------------------------------
"clean": "rimraf dist",
    "imagemin": "imagemin imagenes/* --out-dir dist/images"
-------------------------------------------------------------------------
npm run imagemin
-------------------------------------------------------------------------
npm install --save-dev usemin
-------------------------------------------------------------------------
PARA GRUNT
npm install grunt --save-dev
-------------------------------------------------------------------------
npm install grunt-contrib-sass --save-dev
-------------------------------------------------------------------------
npm install grunt-contrib-watch --save-dev
-------------------------------------------------------------------------
npm install grunt-browser-sync --save-dev
-------------------------------------------------------------------------
PARA CORRERLO
grunt
-------------------------------------------------------------------------
npm install grunt-contrib-imagemin --save-dev
-------------------------------------------------------------------------
npm install time-grunt --save-dev
-------------------------------------------------------------------------
npm install jit-grunt --save-dev
-------------------------------------------------------------------------
npm install grunt-contrib-copy --save-dev
-------------------------------------------------------------------------
npm install grunt-contrib-clean --save-dev
-------------------------------------------------------------------------
npm install grunt-contrib-concat --save-dev
-------------------------------------------------------------------------
npm install grunt-contrib-cssmin --save-dev
-------------------------------------------------------------------------
npm install grunt-contrib-uglify --save-dev
-------------------------------------------------------------------------
npm install grunt-filerev --save-dev
-------------------------------------------------------------------------
npm install grunt-usemin --save-dev